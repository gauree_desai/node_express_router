const express= require("express")
const slashRoutes=require('./routes/slash')
const abcRoutes=require('./routes/abc')

//create server
var app=express()

//const
const PORT=7676
const log=console.log

//add middleware
app.use('/',slashRoutes)
app.use('/abc',abcRoutes)




//listen last stmt
app.listen(PORT,()=>{
  log(`app is running on port ${PORT}`)
})
