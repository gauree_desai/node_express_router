//import
const express= require("express")


//constants
const log=console.log

//create router
const router=express.Router()

router.get('',(req,res)=>{
    res.send("you called get method")
})

router.post('',(req,res)=>{
    res.send("you called post method")
})

router.post('/:id',(req,res)=>{
    log(req.params.id)
    res.send("you called post method")
})

router.put('',(req,res)=>{
    res.send("you called put method")
})

router.delete('',(req,res)=>{
    res.send("you called delete method")
})


//export stmt last stmt
module.exports=router;