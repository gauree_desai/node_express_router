//import
const express= require("express")


//constants
const log=console.log

//create router
const router=express.Router()

// localhost:7676/abc/pqr
router.get('/pqr',(req,res)=>{
    res.send("you called get abc method")
})

router.post('',(req,res)=>{
    res.send("you called post abc method")
})

router.put('',(req,res)=>{
    res.send("you called put abc method")
})

router.delete('',(req,res)=>{
    res.send("you called delete abc method")
})


//export stmt last stmt
module.exports=router;